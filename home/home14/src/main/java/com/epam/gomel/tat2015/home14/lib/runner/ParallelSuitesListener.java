package com.epam.gomel.tat2015.home14.lib.runner;

import com.epam.gomel.tat2015.home14.lib.config.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class ParallelSuitesListener implements ISuiteListener {

    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.config().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalConfig.config().getThreadCount());
    }

    public void onFinish(ISuite suite) {
    }

}
