package com.epam.gomel.tat2015.home14.lib.feature.mail;

import java.io.File;

public class Letter {

    private String recipient;
    private String subject;
    private String body;
    private String ID;
    private File attachment;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public File getAttachment() {
        return attachment;
    }

    public void setAttachment(File attachment) {
        this.attachment = attachment;
    }

    public String getID() {
        return (ID);
    }

    public void setID(String ID) {
        this.ID = ID;
    }

}
