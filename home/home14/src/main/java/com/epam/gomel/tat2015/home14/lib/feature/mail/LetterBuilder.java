package com.epam.gomel.tat2015.home14.lib.feature.mail;

import com.epam.gomel.tat2015.home14.lib.feature.common.CommonConstants;
import com.epam.gomel.tat2015.home14.lib.util.Logger;
import com.epam.gomel.tat2015.home14.lib.util.Randomizer;

public class LetterBuilder {

    public static Letter buildLetter() {
        Logger.info("Building letter");
        Letter letter = new Letter();
        letter.setSubject("test subject " + Randomizer.alphabetic());
        letter.setRecipient(CommonConstants.DEFAULT_USER_LOGIN + "@yandex.ru");
        letter.setBody("mail content " + Randomizer.alphabetic());
        return letter;
    }

    public static Letter buildLetterWithoutProperlyAddress() {
        Logger.info("Building letter without properly address");
        Letter letter = new Letter();
        letter.setSubject("test subject " + Randomizer.alphabetic());
        letter.setRecipient(Randomizer.alphabetic());
        letter.setBody("mail content " + Randomizer.alphabetic());
        return letter;
    }

    public static Letter buildLetterWithBlankSubjectAndBody() {
        Logger.info("Building letter with blank subject and body");
        Letter letter = new Letter();
        letter.setRecipient(CommonConstants.DEFAULT_USER_LOGIN + "@yandex.ru");
        return letter;
    }

    public static Letter buildLetterWithAttachment() {
        Logger.info("Building letter with attachment");
        return null;
    }

}
