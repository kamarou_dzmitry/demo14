package com.epam.gomel.tat2015.home14.lib.util;

import com.epam.gomel.tat2015.home14.lib.config.GlobalConfig;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class ResourceUtils {

    //public static String tempDirectoryPath = FileUtils.getTempDirectoryPath();
    //public static File tempDirectory=new File(GlobalConfig.getInstance().getTempDirectory()+"\\Temp");

    /*ResourceUtils(){
        tempDirectory.mkdir();
    }*/

    public static File getFile() {
        File file = FileUtils.getFile(GlobalConfig.config().getTempDirectory(), Randomizer.alphabetic() + ".txt");
        try {
            file.createNewFile();
            FileUtils.write(file, Randomizer.alphabetic(), "UTF-8");
        } catch (IOException e) {
            Logger.error("Create or write file exception", e);
        }
        return file;
    }

    public static File[] getFiles(int filesCount) {
        File[] files = new File[filesCount];
        for (int i = 0; i < filesCount; i++) {
            files[i] = getFile();
        }
        return files;
    }

    public static File getDownloadDirectory() {
        Logger.debug("Getting download directory");
        File file = FileUtils.getFile(GlobalConfig.config().getTempDirectory(), "Downloads");
        Logger.debug("Make directory "+file.getAbsolutePath());
        file.mkdir();
        Logger.debug("Return: "+file);
        return file;
    }

    public static void deleteDir(String dirName) {
        File directory = new File(dirName);
        if (directory.exists()) {
            directory.delete();
        }
    }

}
