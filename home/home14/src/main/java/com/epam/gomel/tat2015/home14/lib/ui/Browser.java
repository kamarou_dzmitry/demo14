package com.epam.gomel.tat2015.home14.lib.ui;

import com.epam.gomel.tat2015.home14.lib.config.GlobalConfig;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import com.epam.gomel.tat2015.home14.lib.util.Logger;
import com.epam.gomel.tat2015.home14.lib.report.Logger;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import static com.epam.gomel.tat2015.home14.lib.config.GlobalConfig.config;

public class Browser implements WrapsDriver {

    public static final int COMMON_ELEMENT_WAIT_TIME_OUT = 30;
    public static final long WEBDRIVER_IMPLICIT_TIME_OUT = 16;
    private WebDriver driver;
    private String currentURL = "";
    private static Map<Thread, Browser> instances = new HashMap<Thread, Browser>();

    private Browser() {
    }

    public static synchronized Browser rise() {
        Logger.debug("Rise the browser");
        if (instances.get(Thread.currentThread()) != null) {
            instances.get(Thread.currentThread()).quit();
        }
        Browser browser = new Browser();
        browser.createDriver();
        instances.put(Thread.currentThread(), browser);
        return browser;
    }

    public static synchronized Browser current() {
        Logger.debug("Get current browser");
        if (instances.get(Thread.currentThread()) == null) {
            rise();
        }
        return instances.get(Thread.currentThread());
    }

    private void quit() {
        Logger.debug("Quit the browser");
        try {
            WebDriver wrappedDriver = getWrappedDriver();
            if (wrappedDriver != null) {
                wrappedDriver.quit();
            }
        } catch (Exception ignore) {
        } finally {
            instances.remove(Thread.currentThread());
        }
    }

    private void createDriver() {
        Logger.debug("Driver creating");
        if (GlobalConfig.config().getSeleniumHost().equals("")) {
            switch (GlobalConfig.config().getBrowserType()) {
                default:
                    this.driver = localChromeDriver();
                case FIREFOX:
                    this.driver = localFirefoxDriver();
                    break;
                case CHROME:
                    this.driver = localChromeDriver();
                    break;
            }
        } else {
            switch (GlobalConfig.config().getBrowserType()) {
                default:
                    this.driver = remoteChromeDriver(GlobalConfig.config().getSeleniumHost(),
                            GlobalConfig.config().getSeleniumPort());
                case FIREFOX:
                    this.driver = remoteFirefoxDriver(GlobalConfig.config().getSeleniumHost(),
                            GlobalConfig.config().getSeleniumPort());
                    break;
                case CHROME:
                    this.driver = remoteChromeDriver(GlobalConfig.config().getSeleniumHost(),
                            GlobalConfig.config().getSeleniumPort());
                    break;
            }
        }
        driver.manage().window().maximize();
    }

    private WebDriver localFirefoxDriver() {
        Logger.debug("Create local Firefox driver");
        return new FirefoxDriver(setUpFirefox());
    }

    private WebDriver localChromeDriver() {
        Logger.debug("Create local Chrome driver");
        return new ChromeDriver(setUpChrome());
    }

    private DesiredCapabilities setUpFirefox() {
        Logger.debug("Set up Firefox");
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.manager.useWindow", true);
        profile.setPreference("plugin.disable_full_page_plugin_for_types", "application/txt");
        profile.setPreference("browser.download.dir", GlobalConfig.config().getTempDirectory()+"\\Downloads");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/txt;text/plain;text/csv");
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        return capabilities;
    }

    private DesiredCapabilities setUpChrome() {
        Logger.debug("Set up Chrome");
        System.setProperty("webdriver.chrome.driver", GlobalConfig.config().getChromeDriver());
        String downloadFilepath = GlobalConfig.config().getTempDirectory()+"\\Downloads";
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("download.default_directory", downloadFilepath);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("start-maximized");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return capabilities;
    }

    private WebDriver remoteFirefoxDriver(String host, int port) {
        Logger.debug("Create remote Firefox driver");
        try {
            return new RemoteWebDriver(new URL(String.format("%s:%s/wd/hub", host, port)), setUpFirefox());
        } catch (MalformedURLException e) {
            Logger.debug("Remote Firefox exception ", e);
        }
        return null;
    }

    private WebDriver remoteChromeDriver(String host, int port) {
        Logger.debug("Create remote Chrome driver");
        try {
            return new RemoteWebDriver(new URL(String.format("%s:%s/wd/hub", host, port)), setUpChrome());
        } catch (MalformedURLException e) {
            Logger.debug("Remote Chrome exception ", e);
        }
        return null;
    }

    public WebDriver getWrappedDriver() {
        Logger.debug("Getting wrapped driver: "+driver);
        return driver;
    }

    public String getCurrentURL() {
        Logger.debug("Current URL is get");
        return driver.getCurrentUrl();
    }

    public void open(String url) {
        Logger.debug("Open page by URL: " + url);
        getWrappedDriver().get(url);
    }

    public void waitForAppear(By locator) {
        Logger.debug("Waiting for appear by " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForInvisible(By locator) {
        Logger.debug("Waiting invisibility of " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForClickable(By locator) {
        Logger.debug("Waiting of clickable " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void writeText(By locator, String text) {
        Logger.debug("Write '" + text + "' to " + locator);
        waitForAppear(locator);
        if (text == null) {
            return;
        }
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void click(By locator) {
        Logger.debug("Click on the " + locator);
        waitForAppear(locator);
        waitForClickable(locator);
        getWrappedDriver().findElement(locator).click();
    }

    public void doubleClick(By locator) {
        Logger.debug("Double click on the " + locator);
        waitForAppear(locator);
        waitForClickable(locator);
        new Actions(driver).doubleClick(findElement(locator)).build().perform();
    }

    public String getText(By locator) {
        Logger.debug("Getting text of " + locator);
        waitForAppear(locator);
        return getWrappedDriver().findElement(locator).getText();
    }

    public static void screenshot(String mark) {
        Logger.debug("Take a screenshot");
        WebDriver driver = current().getWrappedDriver();
        byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        try {
            FileUtils.writeByteArrayToFile(new File("./src/main/screenshots/" + System.nanoTime() + mark + ".png"), screenshotBytes);
        } catch (Exception e) {
            Logger.error("Screenshot exception", e);
        }
    }

    public static void screenshot() {
        WebDriver driver = current().getWrappedDriver();
        if (isBrowserOpened()) {
            try {
                byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                File screenshotFile = new File(screenshotName());
                FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
                Logger.save(screenshotFile.getName());
            } catch (Exception e) {
                Logger.error("Failed to write screenshot: " + e.getMessage(), e);
            }
        }
    }

    private static String screenshotName() {
        return config().getTempDirectory() + File.separator + System.nanoTime() + ".png";
    }

    private synchronized static boolean isBrowserOpened() {
        Browser browser = browserFotThread();
        return browser != null && browser.getWrappedDriver() != null;
    }

    private static Browser browserFotThread() {
        return instances.get(Thread.currentThread());
    }

    public WebElement findElement(By xpath) {
        Logger.debug("Find element by xpath=" + xpath);
        waitForAppear(xpath);
        return driver.findElement(xpath);
    }

    public By findElement(String xpathString) {
        Logger.debug("Find element by xpath string='" + xpathString + "'");
        waitForAppear(By.xpath(xpathString));
        return By.xpath(xpathString);
    }

    public void move(By is, By to) {
        Logger.debug("Moving " + is + " to " + to);
        WebElement isElement = findElement(is);
        WebElement toElement = findElement(to);
        Action dragToAction = new Actions(driver).dragAndDrop(isElement, toElement).build();
        dragToAction.perform();
    }

    public void select(By locator) {
        Logger.debug("select the " + locator);
        getWrappedDriver().findElement(locator).click();
    }

    public void ctrlDown() {
        Logger.debug("Ctrl press down");
        Actions ctrlPress = new Actions(driver).keyDown(Keys.CONTROL);
        ctrlPress.build().perform();
    }

    public void ctrlUp() {
        Logger.debug("Ctrl press up ");
        Actions ctrlPress = new Actions(driver).keyUp(Keys.CONTROL);
        ctrlPress.build().perform();
    }

}
