package com.epam.gomel.tat2015.home14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.home14.lib.feature.disk.screen.DiskBasePage;
import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class MailboxBasePage {

    private By currentAddressElement = By.xpath("//a[@id='nb-1']");
    public static By composeLetterLink = By.xpath("//a[@href='#compose']");
    public static By deleteButton = By.xpath("//div[contains(@class,'chevron')]/a[8]");
    public static By readButton = By.xpath("//a[@title='Read (q)']");
    public static By diskTab = By.xpath("//a[@title='Disk']");

    public MailboxMenu menu() {
        return new MailboxMenu();
    }

    public WriteLetterPage openWriteLetter() {
        Browser.current().click(composeLetterLink);
        return new WriteLetterPage();
    }

    public InboxPage refresh() {
        return null;
    }

    public String getCurrentAddress() {
        return Browser.current().getText(currentAddressElement).trim();
    }

    public DiskBasePage switchToDiskBasePage() {
        Browser.current().click(diskTab);
        return new DiskBasePage();
    }

}
