package com.epam.gomel.tat2015.home14.lib.feature.mail.service;

import com.epam.gomel.tat2015.home14.lib.feature.mail.Letter;
import com.epam.gomel.tat2015.home14.lib.feature.mail.screen.*;
import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import com.epam.gomel.tat2015.home14.lib.util.Logger;
import com.epam.gomel.tat2015.home14.lib.util.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.Scanner;

public class MailService {

    private WebElement mailRowSubject;

    public boolean isLetterExistsInSent(Letter letter) {
        Logger.debug("Checking exists letter in sent");
        Browser.current().screenshot();
        new MailboxMenu().openSent();
        if ((letter.getSubject() == null) || (letter.getSubject().equals(""))) {
            return findMailRowByID(letter);
        } else {
            return findMailRowBySubject(letter.getSubject());
        }
    }

    public boolean isLetterExistsInInbox(Letter letter) {
        Logger.debug("Checking exists letter in inbox");
        Browser.current().screenshot();
        new MailboxMenu().openInbox();
        if ((letter.getSubject() == null) || (letter.getSubject().equals(""))) {
            return findMailRowByID(letter);
        } else {
            return findMailRowBySubject(letter.getSubject());
        }
    }

    public boolean isLetterExistsInTrash(Letter letter) {
        Logger.debug("Checking exists letter in trash");
        Browser.current().screenshot();
        new MailboxMenu().openTrash();
        if ((letter.getSubject() == null) || (letter.getSubject().equals(""))) {
            return findMailRowByID(letter);
        } else {
            return findMailRowBySubject(letter.getSubject());
        }
    }

    public boolean findMailRowBySubject(String subject) {
        Logger.debug("Find mail row by subject='" + subject + "'");
        Browser.current().screenshot();
        try {
            mailRowSubject = Browser.current().findElement(By.xpath("(//span[@title='" + subject + "'])[1]"));
        } catch (NullPointerException e) {
            Logger.error("mailRowSubject=" + mailRowSubject, e);
            return false;
        }
        Logger.debug("mailRowSubject=" + mailRowSubject);
        return true;
    }

    public boolean findMailRowByID(Letter letter) {
        Logger.debug("Find mail row by ID='" + letter.getID() + "'");
        Browser.current().screenshot();
        try {
            Browser.current().findElement(By.xpath("//a[@href='#message/" + letter.getID() + "']"));
        } catch (NoSuchElementException e) {
            Logger.error("Letter is not found in sent", e);
            return false;
        }
        return true;
    }

    public InboxPage sendLetter(Letter letter) {
        Logger.debug("Sending letter");
        new MailboxBasePage().openWriteLetter().fillField(letter).sendLetter();
        if (checkErrorIsPresent()) {
            return null;
        }
        letter.setID(Utils.cutID(Browser.current().findElement(WriteLetterPage.sendMailStatus).getAttribute("href")));
        Logger.debug("letter.ID=" + letter.getID());
        return new InboxPage();
    }

    public boolean checkErrorIsPresent() {
        Logger.debug("Checking error is present");
        WebElement errorImg = Browser.current().findElement(WriteLetterPage.error);
        Browser.current().screenshot();
        return errorImg.isDisplayed();
    }

    public void deleteLetterFromInbox(Letter letter) {
        Logger.debug("Deleting letter from inbox");
        new MailboxMenu().openInbox();
        selectLetter(letter.getSubject());
        new InboxPage().deleteLetter();
        Browser.current().screenshot();
    }

    public void deleteLetterFromTrash(Letter letter) {
        Logger.debug("Deleting letter from trash");
        new MailboxMenu().openTrash();
        Browser.current().screenshot();
        Browser browser = Browser.current();
        browser.findElement(By.xpath("//input[@value='" + letter.getID() + "']")).click();
        browser.click(MailboxBasePage.readButton);
        browser.findElement(By.xpath("//input[@value='" + letter.getID() + "']")).click();
        browser.click(MailboxBasePage.deleteButton);
        try {
            browser.findElement(By.xpath("//input[@value='" + letter.getID() + "']")).click();
            browser.click(MailboxBasePage.deleteButton);
        } catch (Exception e) {
            Logger.error("Its already deleted", e);
        }
    }

    public void selectLetter(String letterSubject) {
        Logger.debug("Selecting letter '" + letterSubject + "'");
        String followingSiblingString = "//span[@title='" + letterSubject + "']" + "//following-sibling::span";
        By FOLLOWING_SIBLING = By.xpath(followingSiblingString);
        WebElement followingSibling = Browser.current().findElement(FOLLOWING_SIBLING);
        String dataParams = followingSibling.getAttribute("data-params");
        Scanner scanner = new Scanner(dataParams);
        scanner.useDelimiter("=");
        String id1 = "";
        while (scanner.hasNext()) {
            id1 = scanner.next();
        }
        id1 = "t" + id1;
        String checkboxString = "//input[@value='" + id1 + "']";
        By CHECKBOX = By.xpath(checkboxString);
        Browser.current().findElement(CHECKBOX).click();
        Browser.current().screenshot();
    }

}
