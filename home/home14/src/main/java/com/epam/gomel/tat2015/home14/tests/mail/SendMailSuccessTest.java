package com.epam.gomel.tat2015.home14.tests.mail;

import com.epam.gomel.tat2015.home14.tests.BaseTests;
import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.mail.Letter;
import com.epam.gomel.tat2015.home14.lib.feature.mail.LetterBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailService;
import org.testng.annotations.Test;

public class SendMailSuccessTest extends BaseTests {

    Account account = AccountBuilder.getDefaultAccount();
    MailService mailService = new MailService();
    MailLoginService mailLoginService = new MailLoginService();
    Letter letter = LetterBuilder.buildLetter();

    @Test(description = "Send properly mail and check it exist in sent and inbox")
    public void sendAndCheckLetter() {
        mailLoginService.loginToMailbox(account);
        mailService.sendLetter(letter);
        mailService.isLetterExistsInSent(letter);
        mailService.isLetterExistsInInbox(letter);
    }

}
