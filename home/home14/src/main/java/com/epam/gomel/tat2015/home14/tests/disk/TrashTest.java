package com.epam.gomel.tat2015.home14.tests.disk;

import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import com.epam.gomel.tat2015.home14.lib.feature.disk.service.DiskService;
import com.epam.gomel.tat2015.home14.lib.util.ResourceUtils;
import com.epam.gomel.tat2015.home14.tests.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class TrashTest extends BaseTests {

    Account account = AccountBuilder.getDefaultAccount();
    DiskLoginService diskLoginService = new DiskLoginService();
    DiskService diskService = new DiskService();
    File file = null;

    @Test(description = "Check correct uploading generated file to disk by name",
            groups = "downloadDirGroup")
    public void uploadFileAndCheckThatCorrectFileIsUploaded() {
        diskLoginService.loginToDisk(account);
        file = ResourceUtils.getFile();
        diskService.uploadFile(file);
        Assert.assertEquals(diskService.isFileUploaded(file), true, "Check correct file uploaded fail");
    }

    @Test(description = "Check exists deleted file in trash by name",
            dependsOnMethods = "uploadFileAndCheckThatCorrectFileIsUploaded")
    public void deleteFileAndCheckInTrash() {
        diskService.deleteByDragAndDrop(file.getName());
        diskService.checkElementInTrash(file.getName());
    }

    @Test(description = "Check deleted permanently file in trash by message",
            dependsOnMethods = {"uploadFileAndCheckThatCorrectFileIsUploaded", "deleteFileAndCheckInTrash"})
    public void deleteFilePermanentlyAndCheckIt() {
        Assert.assertNotNull(diskService.deleteFileFromTrash(file.getName()), "Check permanently deleted file fail");
    }

}


