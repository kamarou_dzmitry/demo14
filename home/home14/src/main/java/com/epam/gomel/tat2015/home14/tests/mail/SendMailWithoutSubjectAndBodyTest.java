package com.epam.gomel.tat2015.home14.tests.mail;

import com.epam.gomel.tat2015.home14.tests.BaseTests;
import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.mail.Letter;
import com.epam.gomel.tat2015.home14.lib.feature.mail.LetterBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendMailWithoutSubjectAndBodyTest extends BaseTests {

    MailLoginService mailLoginService = new MailLoginService();
    Account account = AccountBuilder.getDefaultAccount();
    Letter letter = LetterBuilder.buildLetterWithBlankSubjectAndBody();
    MailService mailService = new MailService();

    @Test(description = "Send mail without subject and body and check it in Sent and Inbox")
    public void sendBlankSubjectAndBodyLetterCheck() {
        mailLoginService.loginToMailbox(account);
        mailService.sendLetter(letter);
        boolean isExistsInSent = mailService.isLetterExistsInSent(letter);
        boolean isExistsInInbox = mailService.isLetterExistsInInbox(letter);
        Assert.assertEquals(isExistsInSent, true, "Send blank subject and body letter check fail");
        Assert.assertEquals(isExistsInInbox, true, "Send blank subject and body letter check fail");
    }

}
