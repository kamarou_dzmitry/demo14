package com.epam.gomel.tat2015.home14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class LoginFailedPage {

    By errorMessage = By.xpath("//div[@class='error-isle js-error-isle']");

    public String getErrorMassage() {
        return Browser.current().getText(errorMessage);
    }

}
