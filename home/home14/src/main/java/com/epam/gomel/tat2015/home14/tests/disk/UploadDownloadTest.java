package com.epam.gomel.tat2015.home14.tests.disk;

import com.epam.gomel.tat2015.home14.lib.config.GlobalConfig;
import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.common.CommonConstants;
import com.epam.gomel.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import com.epam.gomel.tat2015.home14.lib.feature.disk.service.DiskService;
import com.epam.gomel.tat2015.home14.lib.util.FileOps;
import com.epam.gomel.tat2015.home14.lib.util.ResourceUtils;
import com.epam.gomel.tat2015.home14.tests.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class UploadDownloadTest extends BaseTests {

    Account account = AccountBuilder.getDefaultAccount();
    DiskLoginService diskLoginService = new DiskLoginService();
    DiskService diskService = new DiskService();
    File file=null;

    @Test(description = "Check correct uploading generated file to disk by name")
    public void uploadFileAndCheckThatCorrectFileUploaded() {
        file = ResourceUtils.getFile();
        diskLoginService.loginToDisk(account);
        diskService.uploadFile(file);
        Assert.assertEquals(diskService.isFileUploaded(file), true, "Check correct file uploaded fail");
    }

    @Test(description = "Check correct downloading file to PC", dependsOnMethods = "uploadFileAndCheckThatCorrectFileUploaded")
    public void downloadFileToPCAndCheckFileIsDownloaded() {
        Assert.assertEquals(diskService.downloadSelectedFile(file), true, "Download file to PC and checking downloaded fail");
    }

    @Test(description = "Check correct downloaded file content",
            dependsOnMethods = {"uploadFileAndCheckThatCorrectFileUploaded", "downloadFileToPCAndCheckFileIsDownloaded"})
    public void checkContent() {
        Assert.assertEquals(FileOps.getFileContent(GlobalConfig.config().getTempDirectory()+"\\Downloads\\" + file.getName()),
                FileOps.getFileContent(file.getAbsolutePath()), "Check file content fail");
    }

}