package com.epam.gomel.tat2015.home14.tests.disk;

import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import com.epam.gomel.tat2015.home14.lib.feature.disk.service.DiskService;
import com.epam.gomel.tat2015.home14.lib.util.ResourceUtils;
import com.epam.gomel.tat2015.home14.tests.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;

public class RemoveSeveralElementsAtOnceTest extends BaseTests {

    Account account = AccountBuilder.getDefaultAccount();
    DiskLoginService diskLoginService = new DiskLoginService();
    DiskService diskService = new DiskService();
    File[] files;

    @Parameters({"filesCount"})
    @Test(description = "Check correct uploading generated files to disk by names")
    public void uploadFilesAndCheckThatCorrectFilesAreUploaded(@Optional(value = "4") int filesCount) {
        diskLoginService.loginToDisk(account);
        files = ResourceUtils.getFiles(filesCount);
        diskService.uploadFiles(files);
        Assert.assertEquals(diskService.isFilesUploaded(files), true, "Check correct files uploaded fail");
    }

    @Test(dependsOnMethods = "uploadFilesAndCheckThatCorrectFilesAreUploaded")
    public void deleteUploadedFiles() {
        diskService.moveSelectedFilesToTrash(files);
        Assert.assertEquals(diskService.checkElementsInTrash(files), true, "Check elements in trash fail");
    }

}
