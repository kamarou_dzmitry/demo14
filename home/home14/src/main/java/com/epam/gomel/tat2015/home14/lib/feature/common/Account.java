package com.epam.gomel.tat2015.home14.lib.feature.common;

import com.epam.gomel.tat2015.home14.lib.util.Logger;

public class Account {

    private String login;
    private String password;
    private String email;

    public String getLogin() {
        Logger.info("Getting account login: '" + login + "'");
        return login;
    }

    public void setLogin(String login) {
        Logger.info("Setting account login: '" + login + "'");
        this.login = login;
    }

    public String getPassword() {
        Logger.info("Getting account password: '" + password + "'");
        return password;
    }

    public void setPassword(String password) {
        Logger.info("Setting account password: '" + password + "'");
        this.password = password;
    }

    public String getEmail() {
        Logger.info("Getting account e-mail: '" + email + "'");
        return email;
    }

    public void setEmail(String email) {
        Logger.info("Setting account e-mail: '" + email + "'");
        this.email = email;
    }

}
