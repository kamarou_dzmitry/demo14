package com.epam.gomel.tat2015.home14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.home14.lib.feature.mail.Letter;
import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import com.epam.gomel.tat2015.home14.lib.util.Logger;
import org.openqa.selenium.By;

public class WriteLetterPage extends MailboxBasePage {
    By toInput = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    By subjectInput = By.xpath("//input[@id='compose-subj']");
    By textArea = By.xpath("//textarea[@id='compose-send']");
    By sendButton = By.xpath("//button[@id='compose-submit']");
    public static By saveAndGoButton = By.xpath("//span[contains(text(),'Save and go')]");
    public static By error = By.xpath("(//img[@class='b-mail-icon b-mail-icon_error'])[1]");
    public static By sendMailStatus = By.xpath("//span[@class='b-statusline__content']/a[@href]");

    public WriteLetterPage fillField(Letter letter) {
        Browser browser = Browser.current();
        browser.writeText(toInput, letter.getRecipient());
        browser.writeText(subjectInput, letter.getSubject());
        browser.writeText(textArea, letter.getBody());
        return new WriteLetterPage();
    }

    public void sendLetter() {
        Browser browser = Browser.current();
        browser.click(sendButton);
        try {
            browser.findElement(saveAndGoButton);
            browser.click(saveAndGoButton);
        } catch (Exception e) {
            Logger.debug("'Save and go' button processed.");
        }
    }

}
