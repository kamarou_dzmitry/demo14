package com.epam.gomel.tat2015.home14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.home14.lib.ui.Browser;

public class InboxPage extends MailboxBasePage {

    public InboxPage deleteLetter() {
        Browser.current().click(deleteButton);
        return new InboxPage();
    }

}
