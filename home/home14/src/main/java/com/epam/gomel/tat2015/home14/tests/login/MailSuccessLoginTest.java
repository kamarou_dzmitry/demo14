package com.epam.gomel.tat2015.home14.tests.login;

import com.epam.gomel.tat2015.home14.tests.BaseTests;
import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailLoginService;
import org.testng.annotations.Test;

public class MailSuccessLoginTest extends BaseTests {

    private MailLoginService mailLoginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();

    @Test(description = "Login to mailbox as a valid user")
    public void successLoginToMailbox() {
        mailLoginService.checkSuccessLogin(account);
    }

}
