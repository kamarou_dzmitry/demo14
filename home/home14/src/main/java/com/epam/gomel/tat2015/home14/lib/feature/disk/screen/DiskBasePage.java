package com.epam.gomel.tat2015.home14.lib.feature.disk.screen;

import com.epam.gomel.tat2015.home14.lib.feature.common.CommonConstants;
import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class DiskBasePage {

    public static By uploadButton = By.xpath("//input[@class='button__attach']");
    public static By trash = By.xpath("//div[@data-id='/trash']");

    public UploadFrame typeToUpload(String absoluteFilePath) {
        Browser.current().writeText(uploadButton, absoluteFilePath);
        return new UploadFrame();
    }

    public static DiskBasePage refresh() {
        Browser.current().open(CommonConstants.DISK_URL);
        return new DiskBasePage();
    }

    public static DiskBasePage selectElement(String fileName) {
        Browser.current().click(By.xpath(String.format("//div[@title='%s']", fileName)));
        return new DiskBasePage();
    }

    public static DiskBasePage moveToTrash(String elementName) {
        Browser.current().move(By.xpath(String.format("//div[@title='%s']", elementName)), trash);
        return new DiskBasePage();
    }

    public static TrashPage goToTrash() {
        Browser.current().doubleClick(trash);
        return new TrashPage();
    }

}
