package com.epam.gomel.tat2015.home14.lib.util;

//import ru.yandex.qatools.allure.annotations.Step;

public class Logger {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("com.epam.gomel.tat2015.home14");

    public static void info(String message) {
        //stepLog("INFO " + message);
        logger.info(message);
    }

    public static void info(Object message, Throwable t) {
        //stepLog("INFO " + message);
        logger.info(message, t);
    }

    public static void error(Object message) {
        //stepLog("ERROR " + message);
        logger.error(message);
    }

    public static void error(String message, Throwable t) {
        //stepLog("ERROR " + message);
        logger.error(message, t);
    }

    public static void debug(String message) {
        //stepLog("DEBUG " + message);
        logger.debug(message);
    }

    public static void debug(Object message, Throwable t) {
        //stepLog("DEBUG " + message);
        logger.debug(message, t);
    }

    public static void fatal(Object message) {
        //stepLog("FATAL " + message);
        logger.fatal(message);
    }

    public static void fatal(Object message, Throwable t) {
        //stepLog("FATAL " + message);
        logger.fatal(message, t);
    }

    public static void warn(Object message) {
        //stepLog("WARN " + message);
        logger.warn(message);
    }

    public static void warn(Object message, Throwable t) {
        //stepLog("WARN " + message);
        logger.warn(message, t);
    }

    /*@Step("{0}")
    public static void stepLog(String log) {
    }*/

}
