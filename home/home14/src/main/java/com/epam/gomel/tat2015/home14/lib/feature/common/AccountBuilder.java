package com.epam.gomel.tat2015.home14.lib.feature.common;

import com.epam.gomel.tat2015.home14.lib.util.Logger;
import com.epam.gomel.tat2015.home14.lib.util.Randomizer;

import static com.epam.gomel.tat2015.home14.lib.feature.common.CommonConstants.*;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Logger.info("Creating default account: ");
        Account account = new Account();
        account.setLogin(DEFAULT_USER_LOGIN);
        account.setPassword(DEFAULT_USER_PASSWORD);
        account.setEmail(DEFAULT_USER_MAIL);
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Logger.info("Creating account with wrong password: ");
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + Randomizer.numeric());
        return account;
    }

    public static Account getNonExistedAccount() {
        Logger.info("Creating non-existed account: ");
        Account account = getDefaultAccount();
        account.setLogin(account.getLogin() + Randomizer.numeric());
        return account;
    }

    public static Account getBlankAccount() {
        Logger.info("Creating blank account: ");
        Account account = getDefaultAccount();
        account.setLogin("");
        account.setPassword("");
        return account;
    }

    public static Account getNewAccount(String login, String password) {
        Logger.info("Creating new account: ");
        Account account = getDefaultAccount();
        account.setLogin(login);
        account.setPassword(password);
        return account;
    }

}
