package com.epam.gomel.tat2015.home14.lib.feature.common;

public interface CommonConstants {

    String MAILBOX_URL = "http://mail.yandex.ru";
    String DISK_URL = "https://disk.yandex.ru/client/disk";
    String TRASH_URL = "https://disk.yandex.ru/client/trash";
    String DEFAULT_USER_LOGIN = "dzmitry.kamarou";
    String DEFAULT_USER_PASSWORD = "q1w1e1r1";
    String DEFAULT_USER_MAIL = "dzmitry.kamarou@yandex.ru";
    String DEFAULT_DOWNLOAD_DIR = "C:\\Users\\HozWin\\AppData\\Local\\Temp\\Downloads\\";

}
