package com.epam.gomel.tat2015.home14.lib.runner;

import com.epam.gomel.tat2015.home14.lib.config.GlobalConfig;
import com.epam.gomel.tat2015.home14.lib.util.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import com.epam.gomel.tat2015.home14.lib.report.ExtentReportListener;
import com.epam.gomel.tat2015.home14.lib.report.TestNgLoggerListener;
import org.testng.ITestNGListener;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.testng.xml.Parser;

import static com.epam.gomel.tat2015.home14.lib.config.GlobalConfig.*;

public class Runner {

    public Runner(String args[]) {
        parseCli(args);
    }

    private void parseCli(String[] args) {
        Logger.info("Parse cli params...");
        CmdLineParser parser = new CmdLineParser(GlobalConfig.config());
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            Logger.error("Failed to parse cli params: " + e.getMessage(), e);
            parser.printUsage(System.out);
            System.exit(1);
        }
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        new Runner(args).runTests();
    }

    private void addListeners(TestNG testNG) {
        testNG.setUseDefaultListeners(false);
        List<ITestNGListener> listeners = new ArrayList<ITestNGListener>() {{
            add(new ExtentReportListener());
            //add(new ParallelSuitesListener());
            //add(new TestNgLoggerListener());
            //add(new BrowserQuitListener());
        }};
        for(ITestNGListener listener : listeners) {
            testNG.addListener(listener);
        }
    }

    private void configureSuites(TestNG testNG) throws ParserConfigurationException, SAXException, IOException {
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        for (String suitePath : config().getSuites()) {
            InputStream suiteInClassPath = getSuiteInputStream(suitePath);
            if (suiteInClassPath != null) {
                suites.addAll(new Parser(suiteInClassPath).parse());
            } else {
                suites.addAll(new Parser(suitePath).parse());
            }
        }
        for(XmlSuite xmlSuite : suites) {
            testNG.setCommandLineSuite(xmlSuite);
        }
    }

    private InputStream getSuiteInputStream(String suite) {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(suite);
        return resourceAsStream;
    }

    private void runTests() throws ParserConfigurationException, SAXException, IOException {
        TestNG testNG = new TestNG();
        addListeners(testNG);
        configureSuites(testNG);
      //  testNG.addListener(new CustomTestNgListener());
        testNG.addListener(new ParallelSuitesListener());



        testNG.addListener(new ParallelSuitesListener());
        testNG.addListener(new TestNgLoggerListener());
        //testNG.addListener(new BrowserQuitListener());

        XmlSuite suite = new XmlSuite();
        suite.setName("Suite");
        List<String> files = new ArrayList<String>();
        files.addAll(GlobalConfig.config().getSuites());
        suite.setSuiteFiles(files);
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        testNG.setXmlSuites(suites);
        Logger.info("Tests will be started");
        Logger.info("[BROWSER TYPE]: " + GlobalConfig.config().getBrowserType());
        Logger.info("[CHROME DRIVER]: " + GlobalConfig.config().getChromeDriver());
        Logger.info("[HOST]: " + GlobalConfig.config().getSeleniumHost());
        Logger.info("[PORT]: " + GlobalConfig.config().getSeleniumPort());
        Logger.info("[PARALLEL MODE]: " + GlobalConfig.config().getParallelMode());
        Logger.info("[THREAD COUNT]: " + GlobalConfig.config().getThreadCount());
        Logger.info("[SUITES]: " + GlobalConfig.config().getSuites());
        testNG.run();
    }

}
